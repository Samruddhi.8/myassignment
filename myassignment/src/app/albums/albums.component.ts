import { Component, OnInit } from '@angular/core';
import {ThemePalette} from '@angular/material/core';

export interface Task {
  name: string;
  completed: boolean;
  color: ThemePalette;
  subtasks?: Task[];
}

export interface PeriodicElement {
  name: string;
  position: number;
  email: string;
  address: string;
  action: string;
}

const ELEMENT_DATA: PeriodicElement[] = [
  {position: 1, name: 'Hydrogen', email: 'samruddhi@gmail.com', address: 'Pune', action:'Edit | Delete'},
  {position: 2, name: 'Helium', email: 'samruddhi@gmail.com', address: 'Pune', action:'Edit | Delete'},
  {position: 3, name: 'Lithium', email: 'samruddhi@gmail.com', address: 'Pune', action:'Edit | Delete'},
  {position: 4, name: 'Beryllium', email: 'samruddhi@gmail.com', address: 'Pune', action:'Edit | Delete'},
  {position: 5, name: 'Boron', email: 'samruddhigade@gmail.com', address: 'Pune', action:'Edit | Delete'},
  {position: 6, name: 'Carbon', email: 'samruddhigade@gmail.com', address: 'Pune', action:'Edit | Delete'},
  {position: 7, name: 'Nitrogen', email: 'samruddhigade@gmail.com', address: 'Pune', action:'Edit | Delete'},
  {position: 8, name: 'Oxygen', email: 'samruddhigad@gmail.come', address: 'Pune', action:'Edit | Delete'},
  {position: 9, name: 'Fluorine', email: 'samruddhigade@gmail.com', address: 'Pune', action:'Edit | Delete'},
  {position: 10, name: 'Neon', email: 'samruddhigade@gmail.com', address: 'Pune', action:'Edit | Delete'},
];

@Component({
  selector: 'app-albums',
  templateUrl: './albums.component.html',
  styleUrls: ['./albums.component.css']
})
export class AlbumsComponent {

  // constructor() { }

  // ngOnInit(): void {
  // }

  displayedColumns: string[] = ['position', 'name', 'email', 'address', 'action'];
  dataSource = ELEMENT_DATA;

  // code for checkbox
  task: Task = {
    name: 'Indeterminate',
    completed: false,
    color: 'primary',
    subtasks: [
      {name: 'Primary', completed: false, color: 'primary'},
      {name: 'Accent', completed: false, color: 'accent'},
      {name: 'Warn', completed: false, color: 'warn'}
    ]
  };
  allComplete: boolean = false;

  updateAllComplete() {
    this.allComplete = this.task.subtasks != null && this.task.subtasks.every(t => t.completed);
  }

  someComplete(): boolean {
    if (this.task.subtasks == null) {
      return false;
    }
    return this.task.subtasks.filter(t => t.completed).length > 0 && !this.allComplete;
  }

  setAll(completed: boolean) {
    this.allComplete = completed;
    if (this.task.subtasks == null) {
      return;
    }
    this.task.subtasks.forEach(t => t.completed = completed);
  }

}



