import { Component, OnInit } from '@angular/core';

export interface PeriodicElement {
  name: string;
  position: number;
  email: string;
  address: string;
  action: string;
}

const ELEMENT_DATA: PeriodicElement[] = [
  {position: 1, name: 'Chhaya Gade', email: 'samruddhi@gmail.com', address: 'Pune', action:'Edit | Delete'},
  {position: 2, name: 'Vedant Shirole', email: 'samruddhi@gmail.com', address: 'Pune', action:'Edit | Delete'},
  {position: 3, name: 'Piyush Shirole', email: 'samruddhi@gmail.com', address: 'Pune', action:'Edit | Delete'},
  {position: 4, name: 'Pratiksha tapkir', email: 'samruddhi@gmail.com', address: 'Pune', action:'Edit | Delete'},
  {position: 5, name: 'Hrutuja Ranpise', email: 'samruddhigade@gmail.com', address: 'Pune', action:'Edit | Delete'},
  {position: 6, name: 'Pranay Kambale', email: 'samruddhigade@gmail.com', address: 'Pune', action:'Edit | Delete'},
  {position: 7, name: 'Priya Dethe', email: 'samruddhigade@gmail.com', address: 'Pune', action:'Edit | Delete'},
  {position: 8, name: 'Ketki Deshpande', email: 'samruddhigad@gmail.come', address: 'Pune', action:'Edit | Delete'},
  {position: 9, name: 'Prachi Kashyap', email: 'samruddhigade@gmail.com', address: 'Pune', action:'Edit | Delete'},
  {position: 10, name: 'Neha Singh', email: 'samruddhigade@gmail.com', address: 'Pune', action:'Edit | Delete'},
];

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.css']
})
export class UserComponent{

  // constructor() { }

  // ngOnInit(): void {
  // }

  displayedColumns: string[] = ['position', 'name', 'email', 'address', 'action'];
  dataSource = ELEMENT_DATA;

}






